#! /bin/bash
#
# © 2019 Niels Thykier <niels@thykier.net>
# License: GPL-2+
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e
set -u

. ${DAK_ROOT:?}/integration-tests/common
. ${DAK_ROOT:?}/integration-tests/setup
. ${DAK_ROOT:?}/integration-tests/dinstall

echo "Dinstall::AllowSourceOnlyUploads true;" >> ${DAKBASE}/etc/dak.conf

setup_debian_like_archive
import-fixture-signing-key

# Verify that our dinstall works with an empty archive
dinstall


(
  packages=$(fixture-package-dir)

  upload_changes ${packages:?}/nonfree-package_0.1-1_amd64.changes
  upload_changes ${packages:?}/package_0.1-1_amd64.changes
  upload_changes ${packages:?}/main-contrib-with-debug_0.1-1_amd64.changes
  upload_changes ${packages:?}/binnmupkg_0.1-1_amd64.changes
  upload_changes ${packages:?}/pkgnew_0.1-1_amd64.changes

  process_uploads
)

echo "Verifying that packages in NEW are *not* published"
if ls -l ${DAKBASE}/ftp-master/pool/main/p/package/package_*.dsc 2>/dev/null ; then
    # We never acepted it, so it should not be published!
    echo "package has been publish even though it is still in NEW!?" >&2
    dak ls package
    exit 1
fi

echo "OK; accepting the package and running dinstall to see it become published"

# Accept the package from NEW
(

  dak control-overrides -s unstable -t deb -a << EOF
package required admin
EOF
  dak control-overrides -s unstable -t dsc -a << EOF
package admin
EOF

echo a | dak process-new main-contrib-with-debug_0.1-1_amd64.changes
echo a | dak process-new binnmupkg_0.1-1_amd64.changes
echo a | dak process-new pkgnew_0.1-1_amd64.changes
  
  dak process-new --automatic
  dak process-policy new
)

(
  upload_changes ${packages:?}/main-contrib-with-debug_0.2-1_amd64.changes
  process_uploads

  dak ls main-contrib-with-debug

  to_testing="$(cat <<EOF
contrib-with-debug 0.2-1 amd64
main-contrib-with-debug 0.2-1 source
main-package 0.2-1 all
package 0.1-1 all
package 0.1-1 source
binnmupkg 0.1-1 amd64
binnmupkg 0.1-1 source
EOF
)"

  echo "$to_testing" | dak control-suite -s testing

)

# After running dinstall, the package should now be present in the pool
dinstall
ls -l ${DAKBASE}/ftp-master/pool/main/p/package/package_*.dsc
echo "Published successfully"

(
  upload_changes ${packages:?}/pkgnew_0.1-2~exp_amd64.changes
  process_uploads

  echo a | dak process-new pkgnew_0.1-2~exp_amd64.changes

  dak process-new --automatic
  dak process-policy new

  # pretend the upload to experimental happened (more than) 14 days ago to
  # trigger NVIU issue below
  echo "update override set created = created - interval '14 days' where package like 'pkg%';" | psql

  upload_changes ${packages:?}/pkgnew_0.1-2_source.changes
  process_uploads

  upload_changes ${packages:?}/pkgnew_0.1-2_all.changes
  process_uploads

  upload_changes ${packages:?}/binnmupkg_0.1-2_source.changes
  process_uploads

  upload_changes ${packages:?}/binnmupkg_0.1-2_amd64.changes
  process_uploads

  upload_changes ${packages:?}/binnmupkg_0.1-1+b1_amd64.changes
  process_uploads

  list_all_suites

  # trigger obsolete override issue: needs 2 dinstalls:
  # NVIU is done after obsolete overrides, so we needs a second dinstall to
  # remove the overrides for the packages removed by NVIU
  dinstall
  dinstall

  list_all_suites

  upload_changes ${packages:?}/pkgnew_0.1-2_amd64.changes
  process_uploads

)

list_all_suites

dak ls main-contrib-with-debug

# check that this package has actually been published
ls -l ${DAKBASE}/ftp-master/pool/main/p/pkgnew/pkg-any3_0.1-2_amd64.deb
